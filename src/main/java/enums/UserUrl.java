package enums;

public enum UserUrl {

    PROD("https://n11.com"),
    PREPROD("dev.n11.com");

    public final String url;

    private UserUrl(String url) {
        this.url = url;
    }
}

