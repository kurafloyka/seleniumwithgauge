package util;

import enums.BrowserType;
import enums.TestEnvironment;
import enums.UserUrl;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import java.util.HashMap;

public class WebDriverConfiguration {

    private Logger log = Logger.getLogger(WebDriverConfiguration.class);

    private static WebDriverConfiguration instance;

    private UserUrl url;
    private BrowserType browserType;
    private TestEnvironment testEnvironment = TestEnvironment.DEFAULT;
    private HashMap<String, HashMap<TestEnvironment, By>> identities;


    private WebDriverConfiguration() {
    }

    public static WebDriverConfiguration getInstance() {
        if (instance == null)
            instance = new WebDriverConfiguration();
        return instance;
    }

    public WebDriverConfiguration setBrowserType(BrowserType browserType) {
        this.browserType = browserType;
        return this;
    }

    public WebDriverConfiguration setUrl(UserUrl url) {
        this.url = url;
        return this;
    }

    public BrowserType getBrowserType() {
        return this.browserType;
    }

    public UserUrl getUrl() {
        return this.url;
    }

    public WebDriverConfiguration setTestEnvironment(TestEnvironment testEnvironment) {
        this.testEnvironment = testEnvironment;
        return this;
    }

    public WebDriverConfiguration setIdentities(HashMap<String, HashMap<TestEnvironment, By>> identities) {
        this.identities = identities;
        return this;
    }

    public TestEnvironment getTestEnvironment() {
        return testEnvironment;
    }

    public void printInfo() {
        log.info("******************* Driver Ayarları *******************");
        log.info("Browser Tipi: " + browserType);
        log.info("Test Ortamı: " + testEnvironment);
        log.info("URL: " + url.url);
        log.info("******************* Driver Ayarları *******************");
    }

    public HashMap<String, HashMap<TestEnvironment, By>> getIdentities() {
        return identities;
    }
}
