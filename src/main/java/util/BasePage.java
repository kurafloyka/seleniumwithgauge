package util;

import operations.WaitOperation;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.Set;

public class BasePage {

    private Logger log;
    private WebDriver webDriver;
    private WaitOperation waitOperation;
    private Random random;

    public BasePage() {

        webDriver = WebDriverManager.getInstance().getWebDriver();
        waitOperation = new WaitOperation();
        webDriver = WebDriverManager.getInstance().getWebDriver();
        log = Logger.getLogger(BasePage.class);
        random = new Random();
    }

    public void focusToFrame(By by) {

        try {
            waitOperation.waitUntilReadyForDocumentObjectModel();
            WebElement webElement = waitOperation.waitPresence(by);
            webDriver.switchTo().frame(webElement);
        } catch (Exception exception) {
            String errorMessage = String.format("'%s' framine odaklanırken sorun oluştu. Hata mesajı: %s", by, exception.getMessage());
            log.error(errorMessage);
            Assert.fail(errorMessage);
        }
    }

    public void pressTabOnTheElement(By by) {

        WebElement webElement = waitOperation.waitPresence(by);
        webElement.sendKeys(Keys.TAB);

    }

    public void pressEnterOnTheElement(By by) {

        WebElement webElement = waitOperation.waitPresence(by);
        webElement.sendKeys(Keys.ENTER);
    }

    public void pressEscOnPageObject(By by) {

        WebElement webElement = waitOperation.waitPresence(by);
        webElement.sendKeys(Keys.ESCAPE);
    }

    public String generateRandomNumber(int digitCount) {

        StringBuilder randomNumber = new StringBuilder();
        int max = 10;

        for (int i = 0; i < digitCount; i++) {
            int generatedNumber = random.nextInt(max);

            // ilk hane sıfırdan büyük olmalı
            if (i == 0 && generatedNumber == 0) {
                ++generatedNumber;
            }
            randomNumber.append(generatedNumber);
        }
        return randomNumber.toString();
    }

    public void switchToDefaultContent() {
        webDriver.switchTo().defaultContent();
    }

    public String getText(By locator) {

        waitOperation.waitPresence(locator);
        waitOperation.waitUntilReadyForDocumentObjectModel();
        String textValue = waitOperation.waitVisible(locator).getText().trim();
        if (textValue.isEmpty()) {
            textValue = getAttribute(locator, "value");
        }
        return textValue;
    }

    /**
     * Bu metot, içerisinde metinsel değerler olan String içerisinden sayısal karakterleri ayırt eder.
     * Örnek String: "Askı No : 557878443"
     * Dönen değer: 557878443
     * <p>
     * Örnek String: 1a2b3c4d5e
     * Dönen değer: 12345
     */
    public String getNumericCharacter(String value) {

        StringBuilder numericCharacter = new StringBuilder();
        for (int i = 0; i < value.length(); i++) {

            int currentCharacter = value.charAt(i);

            if (currentCharacter >= 48 && currentCharacter <= 57) {
                numericCharacter.append(value.charAt(i));
            }
        }
        return numericCharacter.toString();
    }

    public String getAttribute(By locator, String attribute) {

        waitOperation.waitUntilReadyForDocumentObjectModel();
        WebElement we = waitOperation.waitPresence(locator);
        return we.getAttribute(attribute) == null ? "" : we.getAttribute(attribute).trim();
    }

    /**
     * Bu metot, String değişkeninizin içerisindeki istemediğiniz karakterleri toplu bir şekilde silmeye yarar.
     *
     * @param dirtyString     Temizlenmesini istediğiniz String
     * @param clearCharacters dirtyString içerisinden temizlemek istediğiniz karakterler
     */
    public String clearCharacter(String dirtyString, String[] clearCharacters) {

        if (dirtyString == null)
            dirtyString = "";

        for (String character : clearCharacters) {
            dirtyString = dirtyString.replace(character, "");
        }
        return dirtyString.trim();
    }

    /**
     * Bu metot güncel tarihi dönderir.
     *
     * @param format Güncel tarihi istediğimiz format örneğin: dd.MM.yyyy
     * @return formatlammış tarih dönderir örnek: 02.04.2020
     */
    public String getCurrentDate(String format) {

        DateFormat dateFormat = new SimpleDateFormat(format);
        Date date = new Date();
        return dateFormat.format(date);
    }

    public void focusToFrameWithIndex(int index) {

        try {
            waitOperation.waitUntilReadyForDocumentObjectModel();
            WaitOperation.wait(5);
            log.info("Sayfada bulunan iframe sayısı : " + webDriver.findElements(By.tagName("iframe")).size());
        } catch (Exception exception) {
            String errorMessage = String.format("'%s' framine odaklanırken sorun oluştu. Hata mesajı: %s", index, exception.getMessage());
            log.error(errorMessage);
            Assert.fail(errorMessage);
        }
    }

    public void pressControlAllOnTheElement(By by) {

        WebElement webElement = waitOperation.waitPresence(by);
        webElement.sendKeys(Keys.chord(Keys.CONTROL, "a"));
    }

    public void pressDownButtonOnElement(By by) {
        WebElement webElement = waitOperation.waitPresence(by);
        webElement.sendKeys(Keys.DOWN);
    }

    public void switchToNewTab() {

        webDriver.switchTo().window(webDriver.getWindowHandle());
        Set<String> winHandles = webDriver.getWindowHandles();
        for (String win : winHandles)
            webDriver.switchTo().window(win);
    }
}
