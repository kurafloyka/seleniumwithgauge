package util;

import enums.TestEnvironment;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;

import java.util.HashMap;


public class IdentitiesManager {

    private Logger log = Logger.getLogger(IdentitiesManager.class);
    private static IdentitiesManager instance;

    private IdentitiesManager() { }

    public static IdentitiesManager getInstance() {
        if(instance == null)
            instance = new IdentitiesManager();
        return instance;
    }

    public By getByFromTestEnvironmentIdentities(String key) {

        HashMap<TestEnvironment, By> elementHashMap = WebDriverConfiguration.getInstance().getIdentities().get(key);

        if (elementHashMap == null) {
            String errorMessage = String.format("'%s' keyi bulunamadı! Lütfen Identities sınıfınızın içerisinde böyle bir key olduğundan emin olun!", key);
            log.error(errorMessage);
            Assert.fail(errorMessage);
        }

        By element = elementHashMap.get(WebDriverConfiguration.getInstance().getTestEnvironment());

        if (element == null)
            element = elementHashMap.get(TestEnvironment.DEFAULT);

        return element;
    }
}
