package util;

import com.thoughtworks.gauge.AfterScenario;
import com.thoughtworks.gauge.BeforeScenario;
import com.thoughtworks.gauge.ExecutionContext;
import com.thoughtworks.gauge.Step;
import enums.BrowserType;
import enums.TestEnvironment;
import enums.UserUrl;
import operations.WaitOperation;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;

import java.util.HashMap;


public class BaseTest {
    private Logger log = Logger.getLogger(BaseTest.class);
    private String currentScenarioName = "";

    @BeforeScenario
    public void init(ExecutionContext context) {
        PropertyConfigurator.configure("src/main/resources/log4j.properties");
        currentScenarioName = context.getCurrentScenario().getName().toUpperCase();
        log.info(String.format("******************** %s SENARYOSU BAŞLADI ********************", currentScenarioName));


        /*logger.info("Current Spec: " + context.getCurrentSpecification().getName());
        logger.info("Current Scenario: " + context.getCurrentScenario().getName());*/

        //testinium url alma
        // locator lari hashmap te tutma
        // options default file alanini tutma
        // bi degeri degiskene atama

        //excel islemleri
        // database islemleri
        //csv islemleri
        // setEnvironmentteki degerleri tut


    }

    @AfterScenario
    public void tearDown() {

        try {
            WebDriverManager.getInstance().getWebDriver().close();
            WebDriverManager.getInstance().getWebDriver().quit();
            WaitOperation.writeTotalStaticTime();
            log.info(String.format("******************** %s SENARYOSU BITTI   ********************", currentScenarioName));
        } catch (Exception ex) {
            log.info(String.format("Driver kapatılırken sorun oluştu hata mesajı: '%s'", ex.getMessage()));
        }
    }


    @Step("Testi başlat")
    public void startTheTest() {

        WebDriverManager
                .getInstance()
                .startTheTest();
    }

    private void setEnvironment(TestEnvironment testEnvironment, BrowserType browserType, HashMap<String, HashMap<TestEnvironment, By>> identities, UserUrl url) {

        WebDriverConfiguration environment = WebDriverConfiguration
                .getInstance()
                .setTestEnvironment(testEnvironment)
                .setBrowserType(browserType)
                .setIdentities(identities)
                .setUrl(url);

        environment.printInfo();

        WebDriverManager
                .getInstance()
                .setWebDriverConfiguration(environment);
    }


    @Step("<url> ortamında çalıştır")
    public void runAtEnvironment(String environment) {

        //switch-case ortamlara gore deger aliyor
        // prod vs icin ortamlari ayarlaniyor


        setEnvironment(TestEnvironment.DEFAULT, BrowserType.CHROME, AllianzIdentities.getAllianzIdentifications(), UserUrl.PROD);

    }

}
