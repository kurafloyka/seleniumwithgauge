package util;

import org.apache.log4j.Logger;

import java.util.HashMap;

public class UserVariable {

    private static HashMap<String, String> userVariables = new HashMap<>();
    private static Logger log = Logger.getLogger(UserVariable.class);

    private UserVariable() { }

    public static void put(String variableName, String variableValue) {
        userVariables.put(variableName, variableValue);
        String logMessage = String.format("'%s' değişkenine '%s' değeri atandı.", variableName, variableValue);
        log.info(logMessage);
    }

    public static String get(String variableName) {
        return userVariables.get(variableName);
    }
}
