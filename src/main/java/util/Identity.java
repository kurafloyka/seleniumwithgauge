package util;

import org.openqa.selenium.By;

public class Identity {

    private By byPrep;
    private By byProd;
    private By byUat;

    public By getByPrep() {
        return byPrep;
    }

    public Identity setByPrep(By byPrep) {
        this.byPrep = byPrep;
        return this;
    }

    public By getByProd() {
        return byProd;
    }

    public Identity setByProd(By byProd) {
        this.byProd = byProd;
        return this;
    }

    public By getByUat() {
        return byUat;
    }

    public Identity setByUat(By byUat) {
        this.byUat = byUat;
        return this;
    }
}
