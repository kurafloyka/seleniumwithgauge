package util;

import enums.BrowserType;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

public class WebDriverManager {
    private static WebDriverManager instance;

    private WebDriver webDriver;
    private WebDriverConfiguration webDriverConfiguration;
    private Logger log = Logger.getLogger(WebDriverManager.class);

    private WebDriverManager() { }

    private DesiredCapabilities capabilities;
    private ChromeOptions options;

    private void chromeOptions() {

        options = new ChromeOptions();
        capabilities = DesiredCapabilities.chrome();
        options.addArguments("--test-type");
        options.addArguments("--disable-popup-blocking");
        options.addArguments("--disable-headless");
        options.addArguments("--disable-notifications");
        options.addArguments("--start-maximized");
        options.addArguments("--lang=tr");
        options.setExperimentalOption("useAutomationExtension", true);
        Map<String, Object> prefs = new HashMap<>();
        prefs.put("browser.helperApps.neverAsk.saveToDisk", "text/csv");
        prefs.put("download.default_directory", "\\\\backupsrv\\Calisma_Gruplari\\OZEL BIRIMLER\\AZTR TEST\\PDF");
        options.setExperimentalOption("prefs", prefs);

        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        capabilities.setBrowserName(options.getBrowserName().toUpperCase());
        capabilities.setVersion(System.getProperty("os.version"));
        capabilities.setPlatform(Platform.getCurrent());
        capabilities.setCapability("acceptSslCerts", "true");
    }

    public static WebDriverManager getInstance() {
        if (instance == null)
            instance = new WebDriverManager();
        return instance;
    }

    public void setWebDriverConfiguration(WebDriverConfiguration webDriverConfiguration) {
        this.webDriverConfiguration = webDriverConfiguration;
    }

    public void startTheTest() {

        if (System.getenv("key") != null) {
            chromeOptions();
            capabilities.setCapability("key", System.getenv("key"));
            try {
                System.setProperty("webdriver.chrome.silentOutput", "true");
                webDriver = new RemoteWebDriver(new URL("http://10.70.6.81:5555/wd/hub"), capabilities);
                SessionId sessionId = ((RemoteWebDriver) webDriver).getSessionId();
                log.info("SessionID:::" + sessionId);
                //writeToLogTestiniumEvironmentIP(sessionId.toString());
            } catch (MalformedURLException e) {
                log.error(e.getMessage());
            }
            webDriverManageSettings();
        }
        else {
            if (webDriverConfiguration.getBrowserType() == BrowserType.CHROME && webDriver == null) {
                chromeOptions();
                java.util.logging.Logger.getLogger("org.openqa.selenium").setLevel(Level.OFF);
                System.setProperty("webdriver.chrome.verboseLogging", "false");
                System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver");
                System.setProperty("webdriver.chrome.silentOutput", "true");
                webDriver = new ChromeDriver(options);
                webDriverManageSettings();
            }
        }
    }

    /*public void writeToLogTestiniumEvironmentIP(String sessionId){

        if (System.getenv("key") != null) {

            String searchQueryApi = "http://10.70.6.81:4444/grid/api/testsession?session=" + sessionId;

            JsonNode body = null;
            try {
                body = Unirest.get(searchQueryApi)
                        .asJson()
                        .getBody();
            }
            catch (UnirestException e) {
                e.printStackTrace();
            }
            JSONObject body2 = body.getObject();
            log.info("Testin çalıştığı Ortam IP : " + body2.getString("proxyId"));
        }
    }*/

    private void webDriverManageSettings() {
        webDriver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
        webDriver.get(webDriverConfiguration.getUrl().url);
    }

    public WebDriver getWebDriver() {

        if (webDriver == null) {
            String errorMessage = "Driver null durumda!";
            log.error(errorMessage);
            Assert.fail(errorMessage);
        }
        return webDriver;
    }
}
