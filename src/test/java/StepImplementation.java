import com.thoughtworks.gauge.Step;
import operations.*;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import util.BasePage;
import util.IdentitiesManager;
import util.UserVariable;
import util.WebDriverManager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class StepImplementation {


    private Logger log;
    private String logMessage = "";

    private BasePage basePage;
    private ClickOperation clickOperation;
    private WaitOperation waitOperation;
    private SendKeysOperation sendKeyOperation;
    private SelectOperation selectOperation;
    private ScrollOperation scrollOperation;
    private AssertionOperation assertionOperation;

    public StepImplementation() {

        basePage = new BasePage();
        log = Logger.getLogger(StepImplementation.class);
        clickOperation = new ClickOperation();
        waitOperation = new WaitOperation();
        sendKeyOperation = new SendKeysOperation();
        selectOperation = new SelectOperation();
        scrollOperation = new ScrollOperation();
        assertionOperation = new AssertionOperation();
    }

    public By getByFromPageElement(String objectId) {

        return IdentitiesManager.getInstance().getByFromTestEnvironmentIdentities(objectId);
    }
    @Step("<key> alanına <value> yaz")
    public void sendKey(String key, String value) {

        By locator = getByFromPageElement(key);
        sendKeyOperation.sendKeys(locator, value);
        logMessage = String.format("'%s' alanına '%s' yazıldı.", key, value);
        log.info(logMessage);
    }

    @Step("<key> alanına JS ile <value> yaz")
    public void sendKeyWithJavaScript(String key, String value) {

        By locator = getByFromPageElement(key);
        sendKeyOperation.sendKeyWithJavaScript(locator, value);
        logMessage = String.format("'%s' alanına '%s' yazıldı.", key, value);
        log.info(logMessage);
    }

    @Step("<key> objesine tıkla")
    public void clickWithJavaScript(String key) {

        By locator = getByFromPageElement(key);
        clickOperation.clickWithJavaScript(locator);
        logMessage = String.format("'%s' objesine tıklandı.", key);
        log.info(logMessage);
    }

    @Step("<key> objesine SE ile tıkla")
    public void clickWithSelenium(String key) {

        By locator = getByFromPageElement(key);
        clickOperation.click(locator);
        logMessage = String.format("'%s' objesine SE ile tıklandı.", key);
        log.info(logMessage);
    }

    @Step("<key> objesine odaklan")
    public void hover(String key) {
        By locator = getByFromPageElement(key);
        clickOperation.hover(locator);
        logMessage = String.format("'%s' objesine odaklanıldı.", key);
        log.info(logMessage);
    }

    @Step("<second> saniye bekle")
    public void wait(int second) {

        WaitOperation.wait(second);
    }

    @Step("<key> frame'ine odaklan")
    public void focusToFrame(String key) {

        By locator = getByFromPageElement(key);
        basePage.focusToFrame(locator);
        logMessage = String.format("'%s' frame odaklanıldı.", key);
        log.info(logMessage);
    }

    @Step("<key> alanını temizle")
    public void clear(String key) {

        By locator = getByFromPageElement(key);
        sendKeyOperation.clear(locator);
        logMessage = String.format("'%s' objesi temizlendi.", key);
        log.info(logMessage);
    }

    @Step("<key> alanına <digitCount> haneli rastgele sayı yaz")
    public void randomNumberFactory(String key, int digitCount) {

        String generatedNumber = basePage.generateRandomNumber(digitCount);
        sendKey(key, generatedNumber);
    }

    @Step("Sayfa üzerinde <text> yazan objeye tıkla")
    public void clickTextOnTheScreen(String text) {

        String xpath = String.format("//*[text()='%s']", text);
        logMessage = String.format("Ekranda '%s' yazan objeye tıklandı.", text);
        By locator = By.xpath(xpath);
        clickOperation.clickWithJavaScript(locator);
        log.info(logMessage);
    }

    @Step("<key> alanına bugünün tarihini <dd/MM/YYYY> formatında yaz")
    public void writeCurrentDateByFormat(String key, String dateStringFormat) {

        DateFormat dateTimeNow = new SimpleDateFormat(dateStringFormat);
        Date date = new Date();
        String now = dateTimeNow.format(date);
        sendKey(key, now);
    }

    @Step("<key> objesinin değerini <variableName> değişkenine kaydet")
    public void saveObjectValueToUserVariable(String key, String variableName) {

        By locator = getByFromPageElement(key);

        String elementText = basePage.getText(locator);
        if (!"".equals(elementText)) {
            elementText = elementText.trim();
        }
        UserVariable.put(variableName, elementText);
    }

    @Step("Sayfa üzerinde <text> değeri görüntülenene kadar bekle")
    public void waitAppearTextOnTheScreen(String text) {

        String xpathString = String.format("//*[contains(text(),'%s')]", text);
        By locator = By.xpath(xpathString);
        waitOperation.waitPresence(locator);
        logMessage = String.format("Sayfa üzerinde '%s' değeri görüntülenene kadar beklendi.", text);
        log.info(logMessage);
    }

    @Step("DEFAULT içeriğe odaklan")
    public void switchToDefaultContent() {

        basePage.switchToDefaultContent();
        log.info("DEFAULT içeriğe geçiş yapıldı");
    }

    @Step("<key> alanına <savedVariableName> değişkeninin değerini yaz")
    public void writeSavedVariableNameToField(String key, String userVariableName) {

        String value = UserVariable.get(userVariableName);
        sendKey(key, value);
    }

    @Step("<key> objesinin değeri <expectedValue> değerini içeriyor mu kontrol et")
    public void implementation4(String key, String expectedValue) {

        By locator = getByFromPageElement(key);
        String actualValue = basePage.getText(locator);
        assertionOperation.containsText(actualValue, expectedValue);
    }

    @Step("<key> objesinin değeri <expectedValue> değişkeninin değerinden farklı mı")
    public void checkElementNotEqualsFromUserVariable(String key, String expectedValue) {

        String actualValue = basePage.getText(getByFromPageElement(key));
        expectedValue = UserVariable.get(expectedValue).replace(" ", "");
        assertionOperation.checkNotEquals(expectedValue, actualValue);
    }

    @Step("<key> objesinin değeri <expectedValue> text değerinden farklı mı")
    public void checkElementNotEquals(String key, String expectedValue) {

        String actualValue = basePage.getText(getByFromPageElement(key));
        expectedValue = expectedValue.replace(" ", "");
        assertionOperation.checkNotEquals(expectedValue, actualValue);
    }

    @Step("<key> objesinin değeri <savedUserVariable> değişken değerini içeriyor mu kontrol et")
    public void implementation5(String key, String savedUserVariable) {

        By locator = getByFromPageElement(key);
        String actualValue = basePage.getText(locator);
        String expectedValue = UserVariable.get(savedUserVariable);
        actualValue = basePage.clearCharacter(actualValue, new String[]{".", "/"});
        expectedValue = basePage.clearCharacter(expectedValue, new String[]{".", "/"});
        assertionOperation.containsText(actualValue, expectedValue);
    }

    @Step("<key> alanı üzerinde TAB tuşuna bas")
    public void pressTabOnPageObject(String key) {

        By locator = getByFromPageElement(key);
        basePage.pressTabOnTheElement(locator);
    }

    @Step("<key> select objesinde <visibleText> değerini seç")
    public void implementation6(String key, String visibleText) {

        By locator = getByFromPageElement(key);
        selectOperation.chooseItemOnTheSelectElementByVisibleText(locator, visibleText);
        logMessage = String.format("'%s' SELECT objesi içerisinde '%s' değerine sahip element seçildi.", key, visibleText);
        log.info(logMessage);
    }

    @Step("<key> select objesinde <index> inci değerini seç")
    public void implementation8(String key, int index) {

        By locator = getByFromPageElement(key);
        selectOperation.chooseItemOnTheSelectElementByIndex(locator, index);
        logMessage = String.format("'%s' SELECT objesi içerisinde '%s' inci element seçildi.", key, index);
        log.info(logMessage);
    }

    @Step("<key> alanına rastgele telefon numarası yaz")
    public void generateRandomCellPhoneNumber(String key) {

        String randomCellPhone = "534" + basePage.generateRandomNumber(7);
        sendKey(key, randomCellPhone);
    }

    @Step("Sayfa üzerinde <key> objesi görüntülenene kadar bekle")
    public void waitUntilVisible(String key) {

        By locator = getByFromPageElement(key);
        waitOperation.waitVisible(locator);
        logMessage = String.format("'%s' objesi sayfa üzerinde görüntülenene kadar beklendi.", key);
        log.info(logMessage);
    }

    @Step("Sayfa üzerinde <key> objesi kaybolana kadar bekle")
    public void waitUntilInvisible(String key) {

        By locator = getByFromPageElement(key);
        waitOperation.waitInvisible(locator);
        logMessage = String.format("Sayfa üzerinde '%s' elementi kaybolana kadar beklendi.", key);
        log.info(logMessage);
    }

    @Step("<key> objesinin tıklanabilir olmasını bekle")
    public void waitUntilForObjectClickable(String key) {

        By locator = getByFromPageElement(key);
        waitOperation.waitClickable(locator, 45);
        logMessage = String.format("'%s' objesinin tıklanabilir olması beklendi.", key);
        log.info(logMessage);
    }

    @Step("Listeden <text> değeri seçilir")
    public void selectElementByText(String listValue) {

        String dropdownValuePath = String.format("//nx-dropdown-item/*[contains(.,'%s')]", listValue);
        String autoCompleteValuePath = String.format("//nx-autocomplete-option/*[contains(.,'%s')]", listValue);
        String mergedXpath = String.format("%s | %s", dropdownValuePath, autoCompleteValuePath);
        By locator = By.xpath(mergedXpath);
        clickOperation.clickWithJavaScript(locator);
        logMessage = String.format("Açılan listeden '%s' değerine tıklandı.", listValue);
        log.info(logMessage);
    }

    @Step("<key> objesi varsa tıkla")
    public void clickIfExist(String key) {

        By locator = IdentitiesManager.getInstance().getByFromTestEnvironmentIdentities(key);
        clickOperation.clickIfExists(locator, 3);
    }

    @Step("JavaScript uyarı mesajında çıkan OK butonuna tıkla")
    public void acceptForJavaScriptAlertPopUp() {

        try {
            WebDriverManager.getInstance().getWebDriver().switchTo().alert().accept();
        } catch (NoAlertPresentException noAlertPresentException) {
            log.warn("JavaScript uyarı mesajı çıkmadı!");
        }
        log.info("JavaScript uyarı mesajında çıkan 'OK' butonuna tıkladı.");
    }

    @Step("<path> dosya yolunda <fileName> dosyası var mı kontrol et")
    public void checkFile(String path, String fileName) {

        boolean isExists = false;
        boolean isEmpty = true;

        for (int i = 0; i < 10; i++) {
            WaitOperation.waitDoNotWriteToLogFile(1);
            isExists = FileOperation.isExists(path, fileName);
            isEmpty = FileOperation.isEmpty(path, fileName);
            if (isExists && !isEmpty)
                break;
        }
        if (!isExists) {
            logMessage = String.format("'%s' dosya yolunda '%s' isimli dosya bulunamadı!", path, fileName);
            log.error(logMessage);
            Assert.fail(logMessage);
        }
        if (isEmpty) {
            logMessage = String.format("'%s' dosya yolunda '%s' isimli dosyanın içi boş!", path, fileName);
            log.error(logMessage);
            Assert.fail(logMessage);
        }
        logMessage = String.format("'%s' dosya yolunda '%s' isimli dosya bulundu.", path, fileName);
        log.info(logMessage);
    }

    @Step("<path> dosya yolunda <fileName> dosyası varsa sil")
    public void removeFileIfExists(String path, String fileName) {

        boolean removed = FileOperation.remove(path, fileName);
        if (removed) {
            logMessage = String.format("'%s' dosya yolunda '%s' isimli dosya silindi!", path, fileName);
            log.info(logMessage);
        }
    }

    @Step("<key> alanı bugünün tarihine eşit mi")
    public void controlTodayDate(String key) {

        String expectedDate = basePage.getCurrentDate("dd.MM.yyy");
        String actualDate;

        By locator = IdentitiesManager.getInstance().getByFromTestEnvironmentIdentities(key);
        String elementValue = basePage.getText(locator);

        actualDate = basePage.clearCharacter(elementValue, new String[]{"\n", "\n", ".", "/", " "});
        expectedDate = basePage.clearCharacter(expectedDate, new String[]{".", "/", " "});

        log.info("Güncel tarih: " + expectedDate);
        log.info("Sayfa üzerinden alınan tarih: " + actualDate);

        if (!expectedDate.equals(actualDate)) {
            logMessage = "Tarih değerleri eşit değil!";
            log.error(logMessage);
            Assert.fail(logMessage);
        }
        log.info("Tarih değerleri eşit");
    }

    @Step("<key> objesi görünene kadar kaydır")
    public void scrollToElement(String objectId) {

        By key = getByFromPageElement(objectId);
        scrollOperation.scrollToElement(key);
        log.info(key + " objesine scroll yapıldı");
    }

    @Step("<key> URL'sine git")
    public void goToTheUrl(String key) {

  /*     String url;
        switch (key.trim()) {
            case "UAT MOTOR THIRD PARTY LIABILITY LOGOUT":
                url = UserUrl.UAT_MOTOR_THIRD_PARTY_LIABILITY_LOGOUT.url;
                break;
            case "UAT MOTOR THIRD PARTY LIABILITY INSURANCE":
                url = UserUrl.UAT_MOTOR_THIRD_PARTY_LIABILITY_INSURANCE.url;
                break;
            case "UAT MOTOR THIRD PARTY LIABILITY SEARCH":
                url = UserUrl.UAT_MOTOR_THIRD_PARTY_LIABILITY_SEARCH.url;
                break;
            case "UAT SIMPLICITY SMALL MEDIUM ENTERPRISES SEARCH":
                url = UserUrl.SEARCH_UAT_SIMPLICITY_SMALL_MEDIUM_ENTERPRISES.url;
                break;
            case "UAT AZNET":
                url = UserUrl.UAT_URL.url;
                break;
            case "PREP ALLIANZ":
                url = UserUrl.PREP_URL.url;
                break;
            case "PREP SIMPLICITY SMALL MEDIUM ENTERPRISES SEARCH":
                url = UserUrl.SEARCH_PREP_SIMPLICITY_SMALL_MEDIUM_ENTERPRISES.url;
                break;
            case "UAT GROUP HEALTH PRODUCTION":
                url = UserUrl.UAT_GROUP_HEALTH_PRODUCTION.url;
                break;
            case "AZNET PREP SIMPLICITY SMALL MEDIUM ENTERPRISES":
                url = UserUrl.AZNET_PREP_SIMPLICITY_SMALL_MEDIUM_ENTERPRISES.url;
                break;
            default:
                url = "";
                break;
        }
        WebDriverManager.getInstance().getWebDriver().get(url);
        logMessage = String.format("'%s' KEY'ine ait '%s' URL'sine yönlendirilme yapıldı.", key, url);
        log.info(logMessage);*/
    }

    //Bu step rgba daki son A codu sık sık değiştiği için RGB kodlarına bakarak kontrol etme işlemini yapıyor
    @Step("<key> objesinin rengi R <0> G <0> B <0> renk kodlarını içerir")
    public void colorControl(String key, String expectedColorRCode, String expectedColorGCode, String expectedColorBCode) {

        String actualColor;
        String expectedColor;

        By locator = getByFromPageElement(key);

        actualColor = waitOperation.waitVisible(locator).getCssValue("background-color");
        expectedColor = String.format("rgba(%s, %s, %s", expectedColorRCode, expectedColorGCode, expectedColorBCode);

        logMessage = String.format("Beklenen RGB kodu: %s", expectedColor);
        log.info(logMessage);
        logMessage = String.format("Alınan RGBA kodu: %s", actualColor);
        log.info(logMessage);

        if (!actualColor.contains(expectedColor)) {
            logMessage = "Renk değerleri eşit değil!";
            log.info(logMessage);
            Assert.fail(logMessage);
        }
        logMessage = String.format("'%s' objesinin renk kodu: R: [%s] G: [%s] B: [%s] kodlarını içeriyor.", key, expectedColorRCode, expectedColorGCode, expectedColorBCode);
        log.info(logMessage);
    }

    @Step("<key> objesinde numara var mı kontrol et")
    public String checkIfThereIsNumberInTheObject(String key) {

        By locator = getByFromPageElement(key);
        String dirtyPoliceNo = basePage.getText(locator);
        String purePoliceNo = basePage.getNumericCharacter(dirtyPoliceNo);
        if (purePoliceNo.isEmpty()) {
            String failMessage = "POLİÇE, ASKI veya TEKLİF numarası bulunamadı!";
            log.error(failMessage);
            Assert.fail(failMessage);
        }
        logMessage = String.format("POLİÇE, ASKI veya TEKLİF numarası: '%s'", purePoliceNo);
        log.info(logMessage);
        return null;
    }

    @Step("<key> objesindeki numarayı <variableName> değişkenine kaydet")
    public void saveNumberAsVariableNameToUserVariable(String objectId, String variableName) {

        String askiNo = checkIfThereIsNumberInTheObject(objectId);
        UserVariable.put(variableName, askiNo);
    }

    @Step("<key> objesinin tıklanabilir olmamasını bekle")
    public void checkIsObjectNotClickable(String key) {

        By locator = getByFromPageElement(key);
        waitOperation.isObjectNotClickable(locator);
        logMessage = String.format("'%s' objesinin tıklanabilir olmaması beklendi.", key);
        log.info(logMessage);
    }

    @Step("<key> objesinin değeri <expectedValue> değerine eşit mi kontrol et")
    public void checkElementsValueEquals(String key, String expectedValue) {

        By locator = getByFromPageElement(key);
        String actualValue = basePage.getText(locator);
        assertionOperation.checkEquals(actualValue, expectedValue);
    }

    @Step("<index> index'li iframe'e odaklan")
    public void focusToFrameWithIndex(int index) {

        basePage.focusToFrameWithIndex(index);
        logMessage = String.format("'%s' frame odaklanıldı.", index);
        log.info(logMessage);
    }

    @Step("<key> objesine kontrol olmadan JS ile tıkla")
    public void clickWithJavaScriptWithNoControl(String objectId) {

        By by = getByFromPageElement(objectId);
        WebDriver webDriver = WebDriverManager.getInstance().getWebDriver();
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) webDriver;
        WaitOperation.waitDoNotWriteToLogFile(2);
        javascriptExecutor.executeScript("arguments[0].click();", webDriver.findElement(by));
        logMessage = String.format("'%s' objesine kontrolsüz tıklandı.", objectId);
        log.info(logMessage);
    }

    @Step("<key> alanı üzerinde CTRL + A tuşuna bas")
    public void saveNumberAsVariableNameToUserVariable(String key) {

        By locator = getByFromPageElement(key);
        basePage.pressControlAllOnTheElement(locator);
    }

    @Step("<key> alanı üzerinde DOWN tuşuna bas")
    public void pressDownInField(String key) {
        By locator = getByFromPageElement(key);
        basePage.pressDownButtonOnElement(locator);
    }

    @Step("<key> alanı üzerinde ENTER tuşuna bas")
    public void pressEnterInField(String key) {
        By locator = getByFromPageElement(key);
        basePage.pressEnterOnTheElement(locator);
    }

    @Step("Yeni açılan sekmeye odaklan")
    public void switchToNewTab() {

        basePage.switchToNewTab();
        log.info("Yeni açılan sekmeye geçiş yapıldı");
    }

    @Step("<key> objesine çift tıkla")
    public void doubleClick(String key) {

        By locator = getByFromPageElement(key);
        clickOperation.doubleClick(locator);
        logMessage = String.format("'%s' objesine çift tıklandı.", key);
        log.info(logMessage);
    }

    @Step("<key> alanı üzerinde ESC tuşuna bas")
    public void pressEscOnPageObject(String key) {

        By locator = getByFromPageElement(key);
        basePage.pressEscOnPageObject(locator);
    }
}
